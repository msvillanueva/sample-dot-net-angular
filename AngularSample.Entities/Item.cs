﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularSample.Entities
{
    public class Item : IEntityBase
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int CategoryID { get; set; }
        public bool Deleted { get; set; }
        public DateTime DateCreated { get; set; }

        public virtual Category Category { get; set; }
    }
}
