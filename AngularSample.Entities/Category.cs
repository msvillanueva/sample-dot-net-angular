﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularSample.Entities
{
    public class Category : IEntityBase
    {
        public Category()
        {
            Items = new List<Item>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }

        public virtual IList<Item> Items { get; set; }
    }
}
