﻿(function (app) {
    'use strict';

    app.controller('itemsCtrl', itemsCtrl);
    itemsCtrl.$inject = ['$scope', '$uibModal', 'apiService', 'notificationService', 'cambria'];

    function itemsCtrl($scope, $uibModal, apiService, notificationService, cambria) {
        $scope.pageClass = 'page-items';
        $scope.loadingData = true;
        $scope.page = 0;
        $scope.pagesCount = 0;
        $scope.items = [];
        $scope.tableRowCollection = [];

        $scope.search = search;
        $scope.clearSearch = clearSearch;
        $scope.create = create;
        $scope.archive = archive;

        function clearSearch() {
            $scope.filter = '';
            search();
        }

        function search() {
            $scope.loadingData = true;
            var config = {
                params: {
                    filter: $scope.filter
                }
            };

            apiService.get('/api/items/', config,
                loadComplete,
                notificationService.responseFailed);
        }

        function loadComplete(response) {
            $scope.loadingData = false;
            $scope.notSynced = response.data.notSynced;
            if ($scope.filter && $scope.filter.length) {
                notificationService.displayInfo(response.data.items.length + (response.data.items.length > 1 ? ' records found' : ' record found'));
            }

            $scope.tableRowCollection = response.data.items;
            $scope.items = [].concat($scope.tableRowCollection);
        }

        function create(item) {
            console.log(item);
            if (item) {
                $scope.viewModel = angular.copy(item);
            }
            else {
                $scope.viewModel = { ID: 0 };
            }
            $uibModal.open({
                templateUrl: 'js/app/items/itemsModal.html',
                controller: 'itemsModalCtrl',
                scope: $scope,
                backdrop: 'static',
                keyboard: false
            }).result.then(function ($scope) {
                clearSearch();
            }, function () {
            });
        }

        function archive(idx, row) {
            cambria.cConfirm('Archive this item?', 'CONFIRM ACTION', function (click) {
                if (click) {
                    $scope.items.splice(idx, 1);
                    apiService.post(
                        '/api/items/remove/',
                        row,
                        function (response) {
                            if (response.data.success)
                                notificationService.displaySuccess(row.Name + ' was removed from the list.');
                            else
                                notificationService.displayError(response.data.message);
                        },
                        notificationService.responseFailed
                    );
                }
            });
        }

        function init() {
            apiService.get('/api/categories/', null,
                function (result) {
                    $scope.categories = result.data.items;
                },
                notificationService.responseFailed);

            $scope.search();
        }

        init();
    }

})(angular.module('cambriaApp'));