﻿(function (app) {
    'use strict';

    app.directive('footerBar', footerBar);
    function footerBar() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'js/app/_layout/footerBar.html'
        }
    }

})(angular.module('cambriaApp'));
