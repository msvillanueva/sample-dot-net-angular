﻿(function (app) {
    'use strict';

    app.controller('rootCtrl', rootCtrl);

    rootCtrl.$inject = ['$scope', '$location', 'membershipService', '$rootScope', 'apiService','notificationService'];
    function rootCtrl($scope, $location, membershipService, $rootScope, apiService, notificationService) {
        $scope.userData = {};
        $scope.itemsByPage = 10;
        $scope.role = 0;
        $scope.username = '';
        $scope.isAdmin = false;

        $scope.displayUserInfo = displayUserInfo;
        $scope.logout = logout;
        $scope.formatDate = formatDate;
        $scope.filterValue = filterValue;
        $scope.setTitle = setTitle;

        function displayUserInfo() {
            $scope.userData.isUserLoggedIn = membershipService.isUserLoggedIn();
            if ($scope.userData.isUserLoggedIn) {
                $scope.username = $rootScope.repository.loggedUser.username;
                $scope.displayName = $rootScope.repository.loggedUser.displayName;
                $scope.role = $rootScope.repository.loggedUser.role;
                $scope.userData.id = $rootScope.repository.loggedUser.id;
            }
            else {
                console.log(window.location);
                //logout();
            }
        }

        function logout() {
            membershipService.removeCredentials();
            window.location.href = 'login.html';
        }

        function formatDate(date) {
            var dateOut = new Date(date);
            return dateOut;
        };

        function filterValue($event) {
            if (isNaN(String.fromCharCode($event.keyCode))) {
                $event.preventDefault();
            }
        };

        function setTitle(title) {
            var subLogo = document.getElementsByClassName('sub-logo')[0];
            if (subLogo)
                document.getElementsByClassName('sub-logo')[0].innerHTML = ' | ' + title;
        }

        $scope.displayUserInfo();
    }

})(angular.module('cambriaApp'));
