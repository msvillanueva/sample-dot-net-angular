﻿using AngularSample.Entities;
using AngularSample.Web.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularSample.Web.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<User, UserViewModel>()
                .ForMember(d => d.FullName, v => v.MapFrom(s => s.FirstName + " " + s.LastName))
                .ForMember(d => d.RoleName, v => v.MapFrom(s => s.Role.Name));
            CreateMap<Category, CategoryViewModel>();
            CreateMap<Item, ItemViewModel>()
                .ForMember(d => d.CategoryName, v => v.MapFrom(s => s.Category.Name));
        }
    }
}