﻿(function (app) {
    'use strict';

    app.directive('noRecord', noRecord);
    function noRecord() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'js/app/_layout/noRecord.html'
        }
    }

})(angular.module('cambriaApp'));
