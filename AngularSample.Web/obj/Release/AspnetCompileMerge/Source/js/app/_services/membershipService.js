﻿(function (app) {
    'use strict';

    app.factory('membershipService', membershipService);

    membershipService.$inject = ['apiService', 'notificationService', '$http', '$base64', '$cookieStore', '$rootScope'];

    function membershipService(apiService, notificationService, $http, $base64, $cookieStore, $rootScope) {
        var baseUrl = 'http://test.cambria.com:56499/';
        //var baseUrl = '';

        var service = {
            login: login,
            saveCredentials: saveCredentials,
            removeCredentials: removeCredentials,
            isUserLoggedIn: isUserLoggedIn,
            encStr: encStr
        }

        function login(user, callback) {
            $http.post(baseUrl + '/api/account/authenticate', user)
                .success(function (response) {
                    callback(response);
                });
        }

        function saveCredentials(user, sUser) {
            console.log('save cre', user, sUser);
            var membershipData = $base64.encode(user.Username + ':' + user.Password);
            $rootScope.repository = {
                loggedUser: {
                    username: sUser.Username,
                    displayName: sUser.Firstname,
                    authdata: membershipData,
                    role: sUser.Role,
                    id: sUser.ID
                }
            };
            $http.defaults.headers.common['Authorization'] = 'Basic ' + membershipData;
            $cookieStore.put('angularDotNetRepo', $rootScope.repository);
        }

        function removeCredentials() {
            $rootScope.repository = {};
            $cookieStore.remove('angularDotNetRepo');
            $http.defaults.headers.common['Authorization'] = '';
            $http.defaults.headers.common['Roles'] = '';
        };

        function loginFailed(response) {
            notificationService.displayError(response.data);
        }

        function isUserLoggedIn() {
            return $rootScope.repository != null && $rootScope.repository.loggedUser != null;
        }

        function encStr(_val) {
            return $base64.encode(_val);
        }

        return service;
    }

})(angular.module('common.core'));