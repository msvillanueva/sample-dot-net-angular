﻿(function (app) {
    'use strict';

    app.directive('loadingSpinner', loadingSpinner);
    function loadingSpinner() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'js/app/_layout/loadingSpinner.html',
            scope: {
                label: '@'
            }
        }
    }

})(angular.module('cambriaApp'));
