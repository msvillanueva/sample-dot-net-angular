﻿(function (app) {
    'use strict';

    app.controller('itemsModalCtrl', itemsModalCtrl);

    itemsModalCtrl.$inject = ['$scope', '$uibModalInstance', '$timeout', 'apiService', 'notificationService'];

    function itemsModalCtrl($scope, $uibModalInstance, $timeout, apiService, notificationService) {
        $scope.close = close;
        $scope.save = save;

        function save() {
            $scope.dataLoading = true;
            apiService.post(
                '/api/items/update/',
                $scope.viewModel,
                saveCompleted,
                notificationService.responseFailed
            );
        }

        function saveCompleted(response) {
            $scope.dataLoading = false;
            if (response.data.success) {

                if ($scope.viewModel.ID == 0) {
                    notificationService.displaySuccess('New item has been added successfully.');
                    $scope.items.push(angular.copy(response.data.item));
                }
                else {
                    notificationService.displaySuccess('The item has been updated successfully.');
                    updateRow();
                }
                $uibModalInstance.dismiss();
            }
            else {
                notificationService.displayError(response.data.message);
            }
        }

        function updateRow() {
            angular.forEach($scope.items, function (row, key) {
                if (row.ID == $scope.viewModel.ID) {
                    row.Name = $scope.viewModel.Name;
                }
            });
        }

        function close() {
            $scope.isEnabled = false;
            $uibModalInstance.dismiss();
        }
    }

})(angular.module('cambriaApp'));