﻿(function () {
    'use strict';

    angular.module('loginApp', ['base64', 'ngCookies', 'common.core', 'common.ui'])
        .run(run);

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http', 'membershipService', '$route'];
    function run($rootScope, $location, $cookieStore, $http, membershipService, $route) {
        $rootScope.repository = $cookieStore.get('angularDotNetRepo') || {};
        if (membershipService.isUserLoggedIn() && $rootScope.repository.loggedUser) {
            $http.defaults.headers.common['Authorization'] = $rootScope.repository.loggedUser.authdata;
            $http.defaults.headers.common['Roles'] = $rootScope.repository.loggedUser.role;
            window.location.href = 'index.html';
        }
        else {
            membershipService.removeCredentials();
        }
    }

    angular.module('loginApp').controller('loginFormCtrl', ['$scope', '$rootScope', '$location', 'membershipService', 'notificationService',
            function ($scope, $rootScope, $location, membershipService, notificationService) {
                $scope.user = {};

                $scope.login = function () {
                    $scope.dataLoading = true;
                    membershipService.removeCredentials();
                    $scope.dataLoading = true;
                    membershipService.login($scope.user, function (response) {
                        if (response.success) {
                            membershipService.saveCredentials($scope.user, response.user);
                            window.location.href = 'index.html';
                        } else {
                            notificationService.displayError('Invalid Login');
                            $scope.user.Password = '';
                            $scope.error = response.message;
                            $scope.dataLoading = false;
                        }
                    });
                };

            }]);


})();