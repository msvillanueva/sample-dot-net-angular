﻿(function () {
    'use strict';

    angular.module('cambriaApp', ['common.core', 'common.ui'])
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider, $scope) {
        $routeProvider
            .when("/", {
                title: 'Items',
                templateUrl: "js/app/items/items.html",
                controller: "itemsCtrl",
                menu: '#menuItems'
            })
            .when("/category", {
                title: 'Item Categories',
                templateUrl: "js/app/category/categories.html",
                controller: "categoriesCtrl",
                menu: '#menuCategories'
            })
            .otherwise({ redirectTo: "/" });
    }

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http', 'membershipService', '$route'];

    function run($rootScope, $location, $cookieStore, $http, membershipService, $route) {
        // handle page refreshes

        // credentials
        $rootScope.repository = $cookieStore.get('angularDotNetRepo') || {};
        if (!membershipService.isUserLoggedIn())
            window.location.href = 'login.html';

        if ($rootScope.repository.loggedUser) {
            $http.defaults.headers.common['Authorization'] = $rootScope.repository.loggedUser.authdata;
            $http.defaults.headers.common['Roles'] = $rootScope.repository.loggedUser.role;
        }

        // route
        $rootScope.$on('$routeChangeSuccess', function () {
            document.title = 'Cambria | ' + $route.current.title;
            var subLogo = document.getElementsByClassName('sub-logo')[0];
            if (subLogo)
                document.getElementsByClassName('sub-logo')[0].innerHTML = ' | ' + $route.current.title;

            var liMenu = document.getElementsByClassName('menu-item');
            angular.forEach(liMenu, function (li, key) {
                angular.element(li).removeClass('active');
            });

            var refElem = angular.element(document.querySelector($route.current.menu));
            if (refElem && refElem.context && refElem.context.nodeName == 'LI') {
                refElem.addClass('active');
            }
        });

        $(document).ready(function () {
            $(".fancybox").fancybox({
                openEffect: 'none',
                closeEffect: 'none'
            });

            $('.fancybox-media').fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    media: {}
                }
            });

            $('[data-toggle=offcanvas]').click(function () {
                $('.row-offcanvas').toggleClass('active');
            });

        });
    }

    isAuthenticated.$inject = ['membershipService', '$rootScope', '$location'];
    function isAuthenticated(membershipService, $rootScope, $location) {
        if (!membershipService.isUserLoggedIn()) {
            $rootScope.previousState = $location.path();
            window.location.href = '/';
        }
    }

})();