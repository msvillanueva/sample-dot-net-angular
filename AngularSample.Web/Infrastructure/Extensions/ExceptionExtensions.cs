﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularSample.Web
{
    public static class ExceptionExtensions
    {
        public static string GetInnerExceptionMsg(this Exception ex)
        {
            return GetInnerEx(ex).Message;
        }

        private static Exception GetInnerEx(Exception ex)
        {
            return ex.InnerException != null ? GetInnerEx(ex.InnerException) : ex;
        }
    }
}