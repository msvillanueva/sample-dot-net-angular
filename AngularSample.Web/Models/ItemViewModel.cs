﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularSample.Web.Models
{
    public class ItemViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int CategoryID { get; set; }
        public bool Deleted { get; set; }
        public DateTime DateCreated { get; set; }

        public string CategoryName { get; set; }
    }
}