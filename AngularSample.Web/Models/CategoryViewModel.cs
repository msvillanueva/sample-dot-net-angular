﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularSample.Web.Models
{
    public class CategoryViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}