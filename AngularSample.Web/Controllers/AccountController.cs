﻿using AngularSample.Data;
using AngularSample.Entities;
using AngularSample.Services;
using AngularSample.Web.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AngularSample.Web.Controllers
{
    [Authorize]
    [RoutePrefix("api/account")]
    public class AccountController : ApiControllerBase
    {
        private readonly IMembershipService _membershipService;
        private readonly IEntityBaseRepository<User> _userRepository;
        private readonly IEncryptionService _encryptionService;

        public AccountController(IEntityBaseRepository<Error> _errorsRepository, IUnitOfWork _unitOfWork,
            IMembershipService membershipService, IEntityBaseRepository<User> userRepository,
            IEncryptionService encryptionService
            ) : base(_errorsRepository, _unitOfWork)
        {
            _membershipService = membershipService;
            _userRepository = userRepository;
            _encryptionService = encryptionService;
        }

        [AllowAnonymous]
        [Route("authenticate")]
        [HttpPost]
        public HttpResponseMessage Login(HttpRequestMessage request, LoginViewModel user)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (ModelState.IsValid)
                {
                    MembershipContext _userContext = _membershipService.ValidateUser(user.Username, user.Password);

                    if (_userContext.User != null)
                    {
                        var userVM = Mapper.Map<UserViewModel>(_userContext.User);
                        response = request.CreateResponse(HttpStatusCode.OK, new { success = true, user = userVM });
                    }
                    else
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, new { success = false });
                    }
                }
                else
                    response = request.CreateResponse(HttpStatusCode.OK, new { success = false });

                return response;
            });
        }
    }
}
