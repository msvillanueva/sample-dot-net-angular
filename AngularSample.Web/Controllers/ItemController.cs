﻿using AngularSample.Data;
using AngularSample.Entities;
using AngularSample.Web.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AngularSample.Web.Controllers
{
    [Authorize]
    [RoutePrefix("api/items")]
    public class ItemController : ApiControllerBase
    {
        private readonly IEntityBaseRepository<Item> _itemRepository;
        private readonly IEntityBaseRepository<Category> _categoryRepository;

        public ItemController(IEntityBaseRepository<Error> _errorsRepository, IUnitOfWork _unitOfWork,
            IEntityBaseRepository<Item> itemRepository, IEntityBaseRepository<Category> categoryRepository
            ) : base(_errorsRepository, _unitOfWork)
        {
            _itemRepository = itemRepository;
            _categoryRepository = categoryRepository;
        }

        [Route("")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                List<Item> items = null;

                items = _itemRepository
                    .FindBy(s => !s.Deleted)
                    .OrderBy(m => m.Name)
                    .ToList();

                var list = Mapper.Map<List<Item>, List<ItemViewModel>>(items);

                response = request.CreateResponse(HttpStatusCode.OK, new { items = list });

                return response;
            });
        }

        [Route("update")]
        [HttpPost]
        public HttpResponseMessage CreateEdit(HttpRequestMessage request, ItemViewModel model)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (ModelState.IsValid)
                {
                    if (model.ID > 0)
                    {
                        var item = _itemRepository.GetSingle(model.ID);
                        if (item != null)
                        {
                            item.Name = model.Name;
                            item.CategoryID = model.CategoryID;
                            _itemRepository.Edit(item);
                            _unitOfWork.Commit();
                        }
                    }
                    else
                    {
                        var item = new Item()
                        {
                            Name = model.Name.Trim(),
                            CategoryID = model.CategoryID,
                            DateCreated = DateTime.Now,
                            Deleted = false
                        };
                        _itemRepository.Add(item);
                        _unitOfWork.Commit();
                        model.ID = item.ID;
                    }

                    model.CategoryName = _categoryRepository.GetSingle(model.CategoryID).Name;

                    response = request.CreateResponse(HttpStatusCode.OK, new { success = true, item = model });
                }
                else
                    response = request.CreateResponse(HttpStatusCode.OK, new { success = false });

                return response;
            });
        }

        [Route("remove")]
        [HttpPost]
        public HttpResponseMessage Archive(HttpRequestMessage request, ItemViewModel model)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (ModelState.IsValid)
                {
                    var item = _itemRepository.GetSingle(model.ID);
                    if (item != null)
                    {
                        item.Deleted = true;
                    }
                    _unitOfWork.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK, new { success = true });
                }
                else
                    response = request.CreateResponse(HttpStatusCode.OK, new { success = false });

                return response;
            });
        }
    }
}
