﻿using AngularSample.Data;
using AngularSample.Entities;
using AngularSample.Web.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AngularSample.Web.Controllers
{
    [Authorize]
    [RoutePrefix("api/categories")]
    public class CategoryController : ApiControllerBase
    {
        private readonly IEntityBaseRepository<Category> _categoryRepository;

        public CategoryController(IEntityBaseRepository<Error> _errorsRepository, IUnitOfWork _unitOfWork,
            IEntityBaseRepository<Category> categoryRepository
            ) : base(_errorsRepository, _unitOfWork)
        {
            _categoryRepository = categoryRepository;
        }

        [Route("")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                List<Category> categories = null;

                categories = _categoryRepository
                    .FindBy(s => !s.Deleted)
                    .OrderBy(m => m.Name)
                    .ToList();

                var list = Mapper.Map<List<Category>, List<CategoryViewModel>>(categories);

                response = request.CreateResponse(HttpStatusCode.OK, new { items = list });

                return response;
            });
        }

        [Route("update")]
        [HttpPost]
        public HttpResponseMessage CreateEdit(HttpRequestMessage request, CategoryViewModel model)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (ModelState.IsValid)
                {
                    if (model.ID > 0)
                    {
                        var category = _categoryRepository.GetSingle(model.ID);
                        if (category != null)
                        {
                            category.Name = model.Name;
                            _categoryRepository.Edit(category);
                            _unitOfWork.Commit();
                        }
                    }
                    else
                    {
                        if (_categoryRepository.FindBy(s => s.Name.Trim().ToLower() == model.Name.Trim().ToLower()).FirstOrDefault() != null)
                            return request.CreateResponse(HttpStatusCode.OK, new { success = false, message = "Category already exists" });

                        var category = new Category()
                        {
                            Name = model.Name.Trim(),
                            Deleted = false
                        };
                        _categoryRepository.Add(category);
                        _unitOfWork.Commit();
                        model.ID = category.ID;
                    }

                    response = request.CreateResponse(HttpStatusCode.OK, new { success = true, item = model });
                }
                else
                    response = request.CreateResponse(HttpStatusCode.OK, new { success = false });

                return response;
            });
        }

        [Route("remove")]
        [HttpPost]
        public HttpResponseMessage Archive(HttpRequestMessage request, CategoryViewModel model)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (ModelState.IsValid)
                {
                    var category = _categoryRepository.GetSingle(model.ID);
                    if (category != null)
                    {
                        category.Deleted = true;
                    }
                    _unitOfWork.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK, new { success = true });
                }
                else
                    response = request.CreateResponse(HttpStatusCode.OK, new { success = false });

                return response;
            });
        }
    }
}
