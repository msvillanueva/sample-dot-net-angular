﻿using AngularSample.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularSample.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("SampleAngular")
        {
            Database.SetInitializer<DatabaseContext>(null);
        }

        #region Entity Sets
        public IDbSet<Error> ErrorSet { get; set; }
        public IDbSet<Role> RoleSet { get; set; }
        public IDbSet<User> UserSet { get; set; }
        public IDbSet<Category> CategorySet { get; set; }
        public IDbSet<Item> ItemSet { get; set; }
        #endregion

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new ErrorConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new ItemConfiguration());
        }
    }
}
