﻿using AngularSample.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularSample.Data
{
    public class RoleConfiguration : EntityBaseConfiguration<Role>
    {
        public RoleConfiguration()
        {
            Property(s => s.ID)
               .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);

            Property(s => s.Name)
                .IsRequired()
                .HasMaxLength(100);

            HasMany(s => s.Users)
                .WithRequired(s => s.Role)
                .HasForeignKey(s => s.RoleID);
        }
    }
}
