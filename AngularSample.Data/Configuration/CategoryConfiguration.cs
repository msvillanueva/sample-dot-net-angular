﻿using AngularSample.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularSample.Data
{
    public class CategoryConfiguration : EntityBaseConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            Property(s => s.Name)
                .IsRequired();

            Property(s => s.Deleted)
                .IsRequired();

            HasMany(s => s.Items)
                .WithRequired(s => s.Category)
                .HasForeignKey(s => s.CategoryID);
        }
    }
}
