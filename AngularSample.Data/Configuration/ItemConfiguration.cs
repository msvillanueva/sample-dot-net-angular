﻿using AngularSample.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularSample.Data
{
    public class ItemConfiguration : EntityBaseConfiguration<Item>
    {
        public ItemConfiguration()
        {
            Property(s => s.Name)
                .IsRequired();

            Property(s => s.CategoryID)
                .IsRequired();

            Property(s => s.Deleted)
                .IsRequired();

            Property(s => s.DateCreated)
                .IsRequired();
        }
    }
}
