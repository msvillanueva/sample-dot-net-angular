﻿using AngularSample.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularSample.Data
{
    public class UserConfiguration : EntityBaseConfiguration<User>
    {
        public UserConfiguration()
        {
            Property(s => s.LastName)
                .IsRequired()
                .HasMaxLength(100);

            Property(s => s.FirstName)
                .IsRequired()
                .HasMaxLength(100);

            Property(s => s.Username)
                .IsRequired()
                .HasMaxLength(20);

            Property(s => s.Email)
                .IsRequired()
                .HasMaxLength(200);

            Property(s => s.HashedPassword)
                .IsRequired()
                .HasMaxLength(255);

            Property(s => s.Salt)
                .IsRequired()
                .HasMaxLength(100);

            Property(s => s.RoleID)
                .IsRequired();

            Property(s => s.IsLocked)
                .IsRequired();

            Property(s => s.DateCreated)
                .IsRequired();

            Property(s => s.Deleted)
                .IsRequired();

        }
    }
}
