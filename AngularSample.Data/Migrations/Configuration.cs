namespace AngularSample.Data.Migrations
{
    using Common;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AngularSample.Data.DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AngularSample.Data.DatabaseContext context)
        {
            context.RoleSet.AddOrUpdate(GenerateRoles());
            context.UserSet.AddOrUpdate(CreateSystemAdmin());
        }

        private Role[] GenerateRoles()
        {
            var roles = new List<Role>();

            var enums = EnumerationHelper.GetEnumList(typeof(Enumerations.Role));
            foreach (var enumItem in enums)
            {
                roles.Add(new Role() { ID = enumItem.ID, Name = enumItem.Name });
            }

            return roles.ToArray();
        }

        private User CreateSystemAdmin()
        {
            var user = new User()
            {
                ID = 1,
                LastName = "Admin",
                FirstName = "Sys",
                Username = "admin",
                Email = "mvsvillanueva@gmail.com",
                HashedPassword = "Kjsdoeb/yS4+AAGr0hBC+QtlFptmKl4t+CmEa7dclCU=", //test
                Salt = "WnUAwGIX44nFd/omrz/RoA==",
                RoleID = 1,
                IsLocked = false,
                Deleted = false,
                DateCreated = DateTime.Now
            };

            return user;
        }
    }
}
