﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularSample.Data
{
    public interface IDbFactory : IDisposable
    {
        DatabaseContext Init();
    }
}
