﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularSample.Common
{
    public class Enumerations
    {
        public enum Role
        {
            [Description("Admin")]
            Admin = 1,

            [Description("User")]
            User = 2
        }
    }
}
